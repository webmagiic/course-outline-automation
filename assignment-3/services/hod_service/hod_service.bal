import ballerina/io;
import ballerina/http;
import ballerinax/kafka;

//import ballerina/uuid;

type User record {|
    int id?;
    string email?;
    string password?;
    string role?;
|};

type Student record {|
    int userId?;
    int studentNo;
    string name;
    string email?;
    string password?;
    string[] courseCodes;
    string role?;
|};

type Lecturer record {|
    int userId?;
    string name;
    string email?;
    string password?;
    int officePhone;
    string officeLocation;
    string officeHours;
    string consultationHours;
    string courseCode;
    string role?;
|};

type Course record {|
    string courseCode;
    string courseName;
    string department;
    string programme;
    string contactHours;
    map<int> students?;
    int lecturerId?;
|};

string filePath = "../files/users.json";

json readJson = check io:fileReadJson(filePath);

map<json> data = check readJson.cloneWithType();

json usersJson = data.get("users");
json lecturersJson = data.get("lecturers");
json studentsJson = data.get("students");
json coursesJson = data.get("courses");

User[] users = check usersJson.cloneWithType();
Lecturer[] lecturers = check lecturersJson.cloneWithType();
Student[] students = check studentsJson.cloneWithType();
Course[] courses = check coursesJson.cloneWithType();

// kafka:ProducerConfiguration producerConfiguration = {
//     clientId: "auth-producer",
//     acks: "all",
//     retryCount: 3
// };

// kafka:Producer authProducer = check new (kafka:DEFAULT_URL);

kafka:ConsumerConfiguration consumerConfiguration = {
    groupId: "authentication-id", // Unique string that identifies the consumer
    offsetReset: "earliest", // Offset reset strategy if no initial offset
    topics: ["authentication"]
};
kafka:Consumer authConsumer = check new (kafka:DEFAULT_URL, consumerConfiguration);

service /hod on new http:Listener(8081) {
    resource function post add_lecturer(@http:Payload Lecturer req) returns json|error {

        kafka:ConsumerRecord[] records = check authConsumer->poll(1);

        // json|error contentJson;
        json contentJson;
        foreach var kafkaRecord in records {
            byte[] content = kafkaRecord.value;
            string contentString = check string:fromBytes(content);
            contentJson = check contentString.fromJsonString();
        }

        User user = check contentJson.cloneWithType();

        // User user;
        // foreach User item in users {
        //     user = item;
        // }

        if user?.role !== "HOD" {
            return {"message": "Anauthorized"};
        }
        User new_user = {id: 2, role: "LECTURER", email: <string>req?.email, password: <string>req?.password};
        Lecturer new_lecturer = {userId: <int>new_user?.id, role: <string>new_user?.role, name: req.name, officePhone: req.officePhone, officeLocation: req.officeLocation, officeHours: req.officeHours, consultationHours: req.consultationHours, courseCode: req.courseCode};

        users.push(new_user);
        lecturers.push(new_lecturer);

        data["users"] = users.toJson();
        data["lecturers"] = lecturers.toJson();
        check io:fileWriteJson(filePath, data);

        return {"message": "Lecturer Added."};

    }

    resource function post add_student(@http:Payload Student req) returns json|error {

        kafka:ConsumerRecord[] records = check authConsumer->poll(1);

        json contentJson;
        foreach var kafkaRecord in records {
            byte[] content = kafkaRecord.value;
            string contentString = check string:fromBytes(content);

            contentJson = check contentString.fromJsonString();

        }

        User user = check contentJson.cloneWithType();

        // User user;
        // foreach User item in users {
        //     user = item;
        // }

        if user?.role !== "HOD" {
            return {"message": "Anauthorized"};
        }
        User new_user = {id: 3, role: "STUDENT", email: <string>req?.email, password: <string>req?.password};
        Student new_student = {userId: <int>new_user?.id, role: <string>new_user?.role, studentNo: req.studentNo, name: req.name, courseCodes: req.courseCodes};

        users.push(new_user);
        students.push(new_student);

        data["users"] = users.toJson();
        data["students"] = lecturers.toJson();
        check io:fileWriteJson(filePath, data);

        return {"message": "Lecturer Added."};

    }

    resource function post add_course(@http:Payload Course req) returns json|error {

        kafka:ConsumerRecord[] records = check authConsumer->poll(1);

        json contentJson;
        foreach var kafkaRecord in records {
            byte[] content = kafkaRecord.value;
            string contentString = check string:fromBytes(content);

            contentJson = check contentString.fromJsonString();

        }
        User user = check contentJson.cloneWithType();

        // User user;
        // foreach User item in users {
        //     user = item;
        // }

        if user?.role !== "HOD" {
            return {"message": "Anauthorized"};
        }

        Course new_course = {courseCode: req.courseCode, courseName: req.courseName, department: req.department, programme: req.programme, contactHours: req.contactHours};

        foreach Lecturer lecturer in lecturers {
            if lecturer.courseCode === new_course.courseCode {
                new_course.lecturerId = <int>lecturer?.userId;
            }
        }

        foreach Student student in students {

            foreach string courseCode in student.courseCodes {

                if courseCode === new_course.courseCode {
                    new_course.students[student.studentNo.toString()] = student.studentNo;
                }
            }
        }

        //users.push(new_user);
        courses.push(new_course);

        //data["users"] = users.toJson();
        data["courses"] = courses.toJson();
        check io:fileWriteJson(filePath, data);

        return {"message": "Lecturer Added."};
    }
}
