import ballerina/uuid;
import ballerina/io;
import ballerina/http;
import ballerinax/kafka;

type User record {|
    int id?;
    string email?;
    string password?;
    string role?;
|};

type CourseOutline record {|
    string id;
    string courseCode?;
    string[] content;
    string[] learning_objectives;
    map<json> schedule;
    string signature?;
    string approval?;
    int[] acknowledgements?;
|};

type Student record {|
    int userId?;
    int studentNo;
    string name;
    string email?;
    string password?;
    string[] courseCode;
    string role?;
|};

type Lecturer record {|
    int userId?;
    string name;
    string email?;
    string password?;
    int officePhone;
    string officeLocation;
    string officeHours;
    string consultationHours;
    string courseCode;
    string role?;
|};

string filePath = "../files/users.json";

json readJson = check io:fileReadJson(filePath);

map<json> data = check readJson.cloneWithType();

json usersJson = data.get("users");
json lecturersJson = data.get("lecturers");
json studentsJson = data.get("students");
json coursesJson = data.get("courses");
json courseOutlinesJson = data.get("courseOutlines");

User[] users = check usersJson.cloneWithType();
Lecturer[] lecturers = check lecturersJson.cloneWithType();
Student[] students = check studentsJson.cloneWithType();
CourseOutline[] courseOutlines = check courseOutlinesJson.cloneWithType();

kafka:ConsumerConfiguration consumerConfiguration = {
    groupId: "authentication-id", // Unique string that identifies the consumer
    offsetReset: "earliest", // Offset reset strategy if no initial offset
    topics: ["authentication"]
};
kafka:Consumer authConsumer = check new (kafka:DEFAULT_URL, consumerConfiguration);

service /course_outline on new http:Listener(8080) {
    resource function post add(@http:Payload CourseOutline req) returns json|error {
        kafka:ConsumerRecord[] records = check authConsumer->poll(1);

        json contentJson;
        foreach var kafkaRecord in records {
            byte[] content = kafkaRecord.value;
            string contentString = check string:fromBytes(content);

            contentJson = check contentString.fromJsonString();

        }
        User user = check contentJson.cloneWithType();

        Lecturer lecturer = {userId: 0, role: "", consultationHours: "", officeHours: "", officeLocation: "", name: "", officePhone: 0, email: "", password: "", courseCode: ""};
        foreach Lecturer item in lecturers {
            if user?.id === item?.userId {
                lecturer = item;
            }
        }

        if user?.role !== "LECTURER" {
            return {"message": "Anauthorized"};
        }

        CourseOutline new_course_outline = {id: uuid:createType1AsString(), courseCode: lecturer.courseCode, content: req.content, learning_objectives: req.learning_objectives, schedule: req.schedule};

        courseOutlines.push(new_course_outline);

        data["courseOutlines"] = courseOutlines.toJson();
        check io:fileWriteJson(filePath, data);

        return {"message": "Lecturer Added."};

    }

    resource function post sign(@http:Payload string signature) returns json|error {
        kafka:ConsumerRecord[] records = check authConsumer->poll(1);

        json contentJson;
        foreach var kafkaRecord in records {
            byte[] content = kafkaRecord.value;
            string contentString = check string:fromBytes(content);

            contentJson = check contentString.fromJsonString();

        }
        User user = check contentJson.cloneWithType();

        Lecturer lecturer = {userId: 0, role: "", consultationHours: "", officeHours: "", officeLocation: "", name: "", officePhone: 0, email: "", password: "", courseCode: ""};
        foreach Lecturer item in lecturers {
            if user?.id === item?.userId {
                lecturer = item;
            }
        }

        if user?.role !== "LECTURER" {
            return {"message": "Anauthorized"};
        }

        foreach int i in 0 ..< courseOutlines.length() {

            if courseOutlines[i]?.courseCode === lecturer.courseCode {
                courseOutlines[i] = {id: courseOutlines[i].id, content: courseOutlines[i].content, schedule: courseOutlines[i].schedule, signature: lecturer.name, learning_objectives: courseOutlines[i].learning_objectives, courseCode: <string>courseOutlines[i]?.courseCode};

                data["courseOutlines"] = courseOutlines.toJson();
                check io:fileWriteJson(filePath, data);

            }
        }

    }

    resource function post approve(@http:Payload string approval) returns json|error {

        kafka:ConsumerRecord[] records = check authConsumer->poll(1);

        json contentJson;
        foreach var kafkaRecord in records {
            byte[] content = kafkaRecord.value;
            string contentString = check string:fromBytes(content);

            contentJson = check contentString.fromJsonString();

        }
        User user = check contentJson.cloneWithType();

        if user?.role !== "HOD" {
            return {"message": "Anauthorized"};
        }

        foreach int i in 0 ..< courseOutlines.length() {

            if user?.role === "HOD" {
                courseOutlines[i] = {id: courseOutlines[i].id, content: courseOutlines[i].content, schedule: courseOutlines[i].schedule, signature: <string>courseOutlines[i]?.signature, approval: <string>user?.role, learning_objectives: courseOutlines[i].learning_objectives, courseCode: <string>courseOutlines[i]?.courseCode};

                data["courseOutlines"] = courseOutlines.toJson();
                check io:fileWriteJson(filePath, data);

            }
        }
    }
    resource function post acknowledge(@http:Payload string ack) returns json|error {
        kafka:ConsumerRecord[] records = check authConsumer->poll(1);

        json contentJson;
        foreach var kafkaRecord in records {
            byte[] content = kafkaRecord.value;
            string contentString = check string:fromBytes(content);

            contentJson = check contentString.fromJsonString();

        }
        User user = check contentJson.cloneWithType();

        Student student = {userId: 0, role: "", name: "", studentNo: 0, email: "", password: "", courseCode: []};
        foreach Student item in students {
            if user?.id === item?.userId {
                student = item;
            }
        }

        if user?.role !== "STUDENT" {
            return {"message": "Anauthorized"};
        }

        foreach int i in 0 ..< courseOutlines.length() {

            foreach Student item in students {

                foreach string courseCode in item.courseCode {

                    if courseOutlines[i]?.courseCode === courseCode {

                        courseOutlines[i] = {id: courseOutlines[i].id, content: courseOutlines[i].content, schedule: courseOutlines[i].schedule, signature: <string>courseOutlines[i]?.signature, approval: <string>courseOutlines[i]?.approval, acknowledgements: [item.studentNo], learning_objectives: courseOutlines[i].learning_objectives, courseCode: <string>courseOutlines[i]?.courseCode};

                        data["courseOutlines"] = courseOutlines.toJson();
                        check io:fileWriteJson(filePath, data);
                    }
                }
            }

        }
    }
}
