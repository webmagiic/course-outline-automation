import ballerina/io;
import ballerina/http;
import ballerinax/kafka;

type Auth record {|
    int id?;
    string email;
    string password;
    string role?;
|};

string filePath = "../files/users.json";

json readJson = check io:fileReadJson(filePath);

map<json> usersMap = check readJson.cloneWithType();

json usersJson = usersMap.get("users");

Auth[] users = check usersJson.cloneWithType();

kafka:ProducerConfiguration producerConfiguration = {
    clientId: "auth-producer",
    acks: "all",
    retryCount: 3
};

kafka:Producer authProducer = check new (kafka:DEFAULT_URL);

service /auth on new http:Listener(8080) {
    resource function post login(@http:Payload Auth req) returns json|error {

        Auth user = {email: "", password: ""};

        foreach int i in 0 ..< users.length() {

            if users[i].email === req.email && users[i].password === req.password {
                user = users[i];
                //return {"message": "Incorrect Email or Password!"};
            }
            else {
                return {"message": "Invalid Email or Password"};
            }
        }

        json result = {"id": user?.id, "email": user.email, "password": user.password, "role": user?.role};
        check authProducer->send({
            topic: "authentication",
            value: result.toJsonString().toBytes()
        });

        check authProducer->'flush();

        return {"message": "Authenticated"};

    }
}
